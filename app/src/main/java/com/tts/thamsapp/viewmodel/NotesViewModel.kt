package com.tts.thamsapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.tts.thamsapp.data.local.entity.NotesEntity
import com.tts.thamsapp.data.repository.NotesRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class NotesViewModel(private val notesRepository: NotesRepository) : ViewModel() {

    private var _added = MutableLiveData<Long>()
    val added: LiveData<Long> get() = _added


    fun addNote(notes: NotesEntity) {
        CoroutineScope(Dispatchers.IO).launch {
            _added.postValue(notesRepository.addNote(notes))
        }
    }

    private var _notes = MutableLiveData<List<NotesEntity>>()
    val notes: LiveData<List<NotesEntity>> get() = notesRepository.getNotes()

    /*fun getNotes() {
        CoroutineScope(Dispatchers.IO).launch {
            _notes.postValue(notesRepository.getNotes().value)
        }
    }*/

    private var _noteById = MutableLiveData<NotesEntity>()
    val noteById: LiveData<NotesEntity> get() = _noteById

    fun getNoteById(id: Long) {
        CoroutineScope(Dispatchers.IO).launch {
            _noteById.postValue(notesRepository.getNoteById(id))
        }
    }

    private var _updatedNote = MutableLiveData<Int>()
    val updatedNote: LiveData<Int> get() = _updatedNote

    fun updateNote(entity: NotesEntity) {
        CoroutineScope(Dispatchers.IO).launch {
            _updatedNote.postValue(
                notesRepository.updateNotesById(
                    title = entity.title,
                    desc = entity.desc,
                    id = entity.id ?: 0
                )
            )
        }
    }

    private var _deletedNote = MutableLiveData<Int>()
    val deletedNote: LiveData<Int> get() = _deletedNote

    fun deleteNote(id: Long) {
        CoroutineScope(Dispatchers.IO).launch {
            _deletedNote.postValue(notesRepository.deleteNoteById(id = id ?: 0))
        }
    }


}