package com.tts.thamsapp.view.home

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.tts.thamsapp.R
import com.tts.thamsapp.databinding.FragmentHomeBinding
import timber.log.Timber

class HomeFragment : Fragment() {

    companion object {
        val name = HomeFragment::class.java.name

    }

    lateinit var binding: FragmentHomeBinding

    override fun onAttach(context: Context) {
        super.onAttach(context)
        Timber.d("LifeCycle Fragment--- Home Fragment : onAttach")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Timber.d("LifeCycle Fragment--- Home Fragment : onCreate")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        Timber.d("LifeCycle Fragment--- Home Fragment : onCreateView")
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Timber.d("LifeCycle Fragment--- Home Fragment : onViewCreated")

        binding.btnUserActivity.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_userActivity)
        }
        binding.cardNotes.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_notesFragment)
        }
        binding.cardWorkManager.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_workManagerFragment)
        }
        binding.cardDialog.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_dialogsFragment)
        }
        binding.cardBroadcast.setOnClickListener {
//            findNavController().navigate(R.id.action_homeFragment_to_broadCastFragment)
            findNavController().navigate(R.id.broadCastFragment)
        }
        binding.cardService.setOnClickListener {
//            findNavController().navigate(R.id.action_homeFragment_to_serviceFragment)
            findNavController().navigate(R.id.serviceFragment)
        }
    }

    override fun onStart() {
        super.onStart()
        Timber.d("LifeCycle Fragment--- Home Fragment : onStart")
    }

    override fun onResume() {
        super.onResume()
        Timber.d("LifeCycle Fragment--- Home Fragment : onResume")
    }

    override fun onPause() {
        super.onPause()
        Timber.d("LifeCycle Fragment--- Home Fragment : onPause")
    }

    override fun onStop() {
        super.onStop()
        Timber.d("LifeCycle Fragment--- Home Fragment : onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        Timber.d("LifeCycle Fragment--- Home Fragment : onDestroy")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Timber.d("LifeCycle Fragment--- Home Fragment : onDestroyView")
    }

    override fun onDetach() {
        super.onDetach()
        Timber.d("LifeCycle Fragment--- Home Fragment : onDetach")
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        Timber.d("LifeCycle Fragment--- Home Fragment : onSaveInstanceState")
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        Timber.d("LifeCycle Fragment--- Home Fragment : onViewStateRestored")

    }
}