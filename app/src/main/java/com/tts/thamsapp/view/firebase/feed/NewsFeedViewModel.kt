package com.tts.thamsapp.view.firebase.feed

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tts.thamsapp.data.model.NewsFeedData
import com.tts.thamsapp.data.service.FirebaseService
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flatMapConcat
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.launch

class NewsFeedViewModel : ViewModel() {

    private val _newsFeedData = MutableLiveData<List<NewsFeedData>>()
    val newsFeedData: LiveData<List<NewsFeedData>> = _newsFeedData

    init {
        viewModelScope.launch {
            FirebaseService.getNewsFeed().collect {
                _newsFeedData.postValue(it)
            }
        }
    }

    suspend fun <T> Flow<List<T>>.flattenToList() = flatMapConcat { it.asFlow() }.toList()
}