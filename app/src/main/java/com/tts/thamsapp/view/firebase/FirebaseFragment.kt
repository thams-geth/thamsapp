package com.tts.thamsapp.view.firebase

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.tts.thamsapp.R
import com.tts.thamsapp.databinding.FragmentFirebaseBinding
import com.tts.thamsapp.utils.visible


class FirebaseFragment : Fragment() {


    lateinit var binding: FragmentFirebaseBinding
    private var auth = Firebase.auth

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentFirebaseBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnGotoSignin.visible(false)
        binding.btnGotoSignin.setOnClickListener {
            findNavController().navigate(R.id.loginPhoneFragment)
        }
    }

    override fun onStart() {
        super.onStart()
        if (auth.currentUser != null) {
            findNavController().popBackStack()
            findNavController().navigate(R.id.newsFeedFragment)
        } else {
            binding.txtInfo.text = getString(R.string.please_login_with_mobile_no)
            binding.btnGotoSignin.visible(true)

        }
    }

}