package com.tts.thamsapp.view.notes

import android.content.Context
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.tts.thamsapp.R
import com.tts.thamsapp.data.local.entity.NotesEntity
import com.tts.thamsapp.databinding.FragmentAddNotesBinding
import com.tts.thamsapp.utils.hideKeyboard
import com.tts.thamsapp.utils.showKeyboard
import com.tts.thamsapp.utils.showToast
import com.tts.thamsapp.utils.toast
import com.tts.thamsapp.viewmodel.NotesViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber


class AddNotesFragment : Fragment() {
    private var noteId: Long? = null
    private lateinit var binding: FragmentAddNotesBinding
    private val notesViewModel: NotesViewModel by viewModel()

    override fun onAttach(context: Context) {
        super.onAttach(context)
        Timber.d("LifeCycle Fragment--- Add Note Fragment : onAttach")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Timber.d("LifeCycle Fragment--- Add Note Fragment : onCreate")
        getBundle()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        Timber.d("LifeCycle Fragment--- Add Note Fragment : onCreateView")
        binding = FragmentAddNotesBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Timber.d("LifeCycle Fragment--- Add Note Fragment : onViewCreated")
        binding.extTitle.requestFocus()
        showKeyboard(binding.extTitle)
        setListeners()
        observeLiveData()
    }

    private fun getBundle() {
        arguments.apply {
            if (this != null) {
                noteId = arguments?.getLong("id")
                notesViewModel.getNoteById(noteId ?: 0)
                setHasOptionsMenu(true)
            } else {
                setHasOptionsMenu(false)
            }
        }
    }
    /*
    private fun getBundle() {
        requireNotNull(arguments).apply {
            val from: CommonOtpNavigationModel? = arguments?.getParcelable("fromPage")
            mUserEmail = from?.userEmail
            mUserMobile = from?.userMobile
            isEntered = from?.isEnteredMobOrEmail
            enteredEmailOrMobileNo = arguments?.getString("enteredEmailOrMobileNo")
        }
    }
    */

    private fun setListeners() {
        binding.fabSave.setOnClickListener {
            if (noteValidation()) {
                val note = NotesEntity(
                    title = binding.extTitle.text.toString().trim(),
                    desc = binding.extDesc.text.toString().trim()
                )
                if (noteId == null)
                    notesViewModel.addNote(note)
                else
                    notesViewModel.updateNote(note.apply { id = noteId })
            }
        }
    }

    private fun observeLiveData() {
        notesViewModel.added.observe(viewLifecycleOwner, {
            if (it > 0) {
                toast(requireContext(), "Note Added")
                findNavController().navigateUp()
            }
        })
        notesViewModel.noteById.observe(viewLifecycleOwner, { note ->
            if (note != null) {
                binding.extTitle.setText(note.title)
                binding.extDesc.setText(note.desc)
            }
        })
        notesViewModel.updatedNote.observe(viewLifecycleOwner, { note ->
            if (note != null) {
                toast(requireContext(), "Note Updated")
                findNavController().navigateUp()
            }
        })
        notesViewModel.deletedNote.observe(viewLifecycleOwner, { note ->
            if (note != null) {
                toast(requireContext(), "Note Deleted")
                findNavController().navigateUp()
            }
        })
    }

    private fun noteValidation(): Boolean {
        var valid = true
        if (binding.extTitle.text.toString().trim() == "") {
            binding.extTitle.error = "Please enter title"
            valid = false
        }
        if (binding.extDesc.text.toString().trim() == "") {
            binding.extDesc.error = "Please enter description"
            valid = false
        }
        return valid
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        return inflater.inflate(R.menu.menu_add_notes, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_delete -> {
                notesViewModel.deleteNote(noteId ?: 0)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onStart() {
        super.onStart()
        Timber.d("LifeCycle Fragment--- Add Note Fragment : onStart")
    }

    override fun onResume() {
        super.onResume()
        Timber.d("LifeCycle Fragment--- Add Note Fragment : onResume")
    }

    override fun onPause() {
        hideKeyboard(binding.root)
        super.onPause()
        Timber.d("LifeCycle Fragment--- Add Note Fragment : onPause")
    }

    override fun onStop() {
        super.onStop()
        Timber.d("LifeCycle Fragment--- Add Note Fragment : onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        Timber.d("LifeCycle Fragment--- Add Note Fragment : onDestroy")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Timber.d("LifeCycle Fragment--- Add Note Fragment : onDestroyView")
    }

    override fun onDetach() {
        super.onDetach()
        Timber.d("LifeCycle Fragment--- Add Note Fragment : onDetach")
    }
}