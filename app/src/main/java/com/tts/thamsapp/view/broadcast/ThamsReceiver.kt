package com.tts.thamsapp.view.broadcast

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import com.tts.thamsapp.utils.toast

class ThamsReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        // This method is called when the BroadcastReceiver is receiving an Intent broadcast.
        val pendingResult: PendingResult = goAsync()
        val myTask = MyTask(intent, pendingResult)
        toast(context, "Action : ${intent.action}")
        myTask.execute()
    }

    private class MyTask(private val intent: Intent, private val pendingResult: PendingResult) :
        AsyncTask<String, Int, String>() {
        override fun doInBackground(vararg p0: String?): String {
            val sb = StringBuilder()
            sb.append("Action: ${intent.action}\n")
            sb.append("URI: ${intent.toUri(Intent.URI_INTENT_SCHEME)}\n")
            return toString()
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            pendingResult.finish()
        }


    }
}