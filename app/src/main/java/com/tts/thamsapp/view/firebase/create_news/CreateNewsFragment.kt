package com.tts.thamsapp.view.firebase.create_news

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import androidx.work.WorkRequest
import androidx.work.workDataOf
import com.bumptech.glide.Glide
import com.tts.thamsapp.R
import com.tts.thamsapp.databinding.FragmentCreateNewsBinding
import com.tts.thamsapp.utils.PathFinder
import com.tts.thamsapp.utils.toast
import com.tts.thamsapp.utils.visible
import com.tts.thamsapp.view.firebase.work_manager.UploadNewsWorker
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.io.File

class CreateNewsFragment : Fragment() {

    companion object {
        fun newInstance() = CreateNewsFragment()

        val permissions = arrayOf(
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
        )
    }

    private val viewModel: CreateNewsViewModel by viewModel()
    private lateinit var binding: FragmentCreateNewsBinding
    private lateinit var imageUri: Uri
    private lateinit var imagePath: String


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCreateNewsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
    }

    private fun setListeners() {
        binding.btnPostNews.setOnClickListener {
            if (validateNews()) {
                uploadNews()
            }
        }
        binding.cardImage.setOnClickListener {
            if (hasPermissions()) {
                showImagePickOption()
            } else {
                requestMultiplePermissions.launch(permissions)
            }
        }
        binding.tilNewsTitle.editText?.addTextChangedListener {
            if (it.toString().isEmpty()) {
                binding.tilNewsTitle.error = "Please enter news title"
            } else {
                binding.tilNewsTitle.error = null
            }
        }
        binding.tilNewsContent.editText?.addTextChangedListener {
            if (it.toString().isEmpty()) {
                binding.tilNewsContent.error = "Please enter news content"
            } else {
                binding.tilNewsContent.error = null
            }
        }
    }

    private fun hasPermissions(): Boolean {
        for (permission in permissions) {
            if (ContextCompat.checkSelfPermission(requireContext(), permission) != PackageManager.PERMISSION_GRANTED) {
                return false
            }
        }
        return true
    }

    private val requestMultiplePermissions =
        registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { permissions ->
            var allPermissionAdded = true
            permissions.entries.forEach {
                println("Permission key and value ${it.key} = ${it.value}")
                if (!it.value) allPermissionAdded = false
            }
            if (allPermissionAdded) {
                showImagePickOption()
            }
        }

    private fun showImagePickOption() {
        val alert = AlertDialog.Builder(requireContext())
            .setTitle("Select any")
            .setItems(R.array.option) { dialogInterface, i ->
                dialogInterface.dismiss()
                when (i) {
                    0 -> openCamera()
                    1 -> openGallery()
                    2 -> openGooglePhotos()
                    else -> dialogInterface.dismiss()
                }
            }
        alert.create().show()
    }

    // Camera
    private fun openCamera() {
        val photoFile = File.createTempFile(
            "IMG_${System.currentTimeMillis()}",
            ".jpg",
            requireContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        ).apply {
            imagePath = absolutePath
        }
        imageUri = FileProvider.getUriForFile(
            requireContext(), "${requireContext().packageName}.provider", photoFile
        )
        takePictureLauncher.launch(imageUri)
    }

    private val takePictureLauncher = registerForActivityResult(ActivityResultContracts.TakePicture()) {
        if (it) {
            Glide.with(requireContext()).load(imageUri).into(binding.ivNews)
            binding.txtImageError.visible(false)
        }
    }

    // Gallery
    private fun openGallery() {
        Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI).also {
            galleryLauncher.launch(it)
        }
    }

    private val galleryLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        if (it.resultCode == Activity.RESULT_OK) {
            imageUri = it.data?.data as Uri
            Glide.with(requireContext()).load(imageUri).into(binding.ivNews)
            binding.txtImageError.visible(false)
            imagePath = PathFinder.getPathFromURI(requireContext(), imageUri) ?: ""
        }
    }

    // Google photos
    private fun openGooglePhotos() {
        Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI).apply {
            setPackage("com.google.android.apps.photos")
            if (resolveActivity(requireActivity().packageManager) != null)
                galleryLauncher.launch(this)
            else
                toast(requireContext(), "You don't have google photos app. Please install and try")
        }
    }

    // Upload to firebase
    private fun uploadNews() {
        // /raw//storage/emulated/0/DCIM/Camera/IMG_20211129_234358.jpg
        // /my_images/IMG_16407077522809067231903680309682.jpg
        val uploadNewsBuilder: WorkRequest = OneTimeWorkRequestBuilder<UploadNewsWorker>()
            .setInputData(
                workDataOf(
                    "title" to binding.tilNewsTitle.editText?.text?.trim().toString(),
                    "content" to binding.tilNewsContent.editText?.text?.trim().toString(),
                    "imagePath" to imagePath
                )
            )
            .build()
        toast(requireContext(), "Your upload is in progress.We will notify once the image uploaded.")
        WorkManager.getInstance(requireContext()).enqueue(uploadNewsBuilder)
    }

    private fun validateNews(): Boolean {
        var valid = true
        if (binding.tilNewsTitle.editText?.text.toString().trim().isEmpty()) {
            binding.tilNewsTitle.error = "Please enter news title"
            valid = false
        }
        if (binding.tilNewsContent.editText?.text.toString().trim().isEmpty()) {
            binding.tilNewsContent.error = "Please enter news content"
            valid = false
        }
        if (!::imageUri.isInitialized) {
            binding.txtImageError.visible(true)
            valid = false
        } else {
            binding.txtImageError.visible(false)
        }
        return valid
    }

}