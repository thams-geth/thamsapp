package com.tts.thamsapp.view.broadcast

import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.tts.thamsapp.databinding.FragmentBroadCastBinding

class BroadCastFragment : Fragment() {

    lateinit var binding: FragmentBroadCastBinding
    lateinit var thamsReceiver: ThamsReceiver
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_broad_cast, container, false)
        binding = FragmentBroadCastBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
        thamsReceiver = ThamsReceiver()
        val intent = IntentFilter().apply {
            addAction(Intent.ACTION_POWER_CONNECTED)
            addAction(Intent.ACTION_POWER_DISCONNECTED)
        }
        requireContext().registerReceiver(thamsReceiver, intent)
//        requireContext().sendBroadcast()

    }

    override fun onPause() {
        super.onPause()
        if (this::thamsReceiver.isInitialized)
            requireContext().unregisterReceiver(thamsReceiver)
    }
}