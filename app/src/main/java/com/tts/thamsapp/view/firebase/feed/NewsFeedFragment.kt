package com.tts.thamsapp.view.firebase.feed

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.tts.thamsapp.R
import com.tts.thamsapp.data.model.NewsFeedData
import com.tts.thamsapp.databinding.FragmentNewsFeedBinding
import com.tts.thamsapp.utils.toast
import com.tts.thamsapp.view.firebase.feed.adapter.NewsFeedListAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel

class NewsFeedFragment : Fragment() {

    companion object {
        fun newInstance() = NewsFeedFragment()
    }

    private val auth = Firebase.auth

    lateinit var binding: FragmentNewsFeedBinding

    //    private lateinit var viewModel: NewsFeedViewModel
    lateinit var newsFeedListAdapter: NewsFeedListAdapter
    private val newsFeedViewModel: NewsFeedViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
//        viewModel = ViewModelProvider(this)[NewsFeedViewModel::class.java]
        binding = FragmentNewsFeedBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
        setAdapter()
        observeLiveData()
    }

    private fun setListeners() {
        binding.txtLogout.setOnClickListener {
            auth.signOut()
            findNavController().navigateUp()
        }
        binding.fabCreateNews.setOnClickListener {
            findNavController().navigate(R.id.createNewsFragment)
        }
    }

    private fun observeLiveData() {
        newsFeedViewModel.newsFeedData.observe(viewLifecycleOwner, {
            newsFeedListAdapter.submitList(it)
        })
    }

    private fun setAdapter() {
        newsFeedListAdapter = NewsFeedListAdapter(newsFeedClick)
        binding.rvNews.adapter = newsFeedListAdapter
    }

    private val newsFeedClick = { data: NewsFeedData ->
//        Intent(this, MapsActivity::class.java).apply {
//            putExtra("latitude", data.latitude)
//            putExtra("longitude", data.longitude)
//            startActivity(this)
//        }
        toast(requireContext(), data.title.toString())
    }

}