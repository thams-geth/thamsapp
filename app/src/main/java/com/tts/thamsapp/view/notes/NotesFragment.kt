package com.tts.thamsapp.view.notes

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.tts.thamsapp.R
import com.tts.thamsapp.databinding.FragmentNotesBinding
import com.tts.thamsapp.utils.hideKeyboard
import com.tts.thamsapp.view.notes.adapter.NotesListAdapter
import com.tts.thamsapp.viewmodel.NotesViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class NotesFragment : Fragment() {


    private lateinit var binding: FragmentNotesBinding
    private val notesViewModel: NotesViewModel by viewModel()
    private var notesAdapter: NotesListAdapter? = null
//    private var notesList = listOf<NotesEntity>()

    override fun onAttach(context: Context) {
        super.onAttach(context)
        Timber.d("LifeCycle Fragment--- Notes Fragment : onAttach")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Timber.d("LifeCycle Fragment--- Notes Fragment : onCreate")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        Timber.d("LifeCycle Fragment--- Notes Fragment : onCreateView")
//        notesViewModel = ViewModelProvider(this).get(NotesViewModel::class.java)
        binding = FragmentNotesBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Timber.d("LifeCycle Fragment--- Notes Fragment : onViewCreated")
        setListeners()
        setNoteAdapter()
        observeLiveData()
//        notesViewModel.getNotes()
    }

    private fun setListeners() {
        binding.fabAdd.setOnClickListener {
            findNavController().navigate(R.id.action_notesFragment_to_addNotesFragment)
        }
    }

    private fun observeLiveData() {
        notesViewModel.notes.observe(viewLifecycleOwner, {
            if (it != null && it.isNotEmpty()) {
//                notesList = it
                notesAdapter?.submitList(it)
            }
        })
    }

    private fun setNoteAdapter() {
        notesAdapter = NotesListAdapter(onNoteClick = noteClick)
        binding.rvNotes.adapter = notesAdapter
    }

    private val noteClick = { noteId: Long ->
        val bundle = bundleOf(
            "id" to noteId,
        )
        findNavController().navigate(R.id.action_notesFragment_to_addNotesFragment, bundle)
    }

    override fun onStart() {
        super.onStart()
        Timber.d("LifeCycle Fragment--- Notes Fragment : onStart")
    }

    override fun onResume() {
        super.onResume()
        Timber.d("LifeCycle Fragment--- Notes Fragment : onResume")
    }

    override fun onPause() {
        hideKeyboard(binding.root)
        super.onPause()
        Timber.d("LifeCycle Fragment--- Notes Fragment : onPause")
    }

    override fun onStop() {
        super.onStop()
        Timber.d("LifeCycle Fragment--- Notes Fragment : onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        Timber.d("LifeCycle Fragment--- Notes Fragment : onDestroy")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Timber.d("LifeCycle Fragment--- Notes Fragment : onDestroyView")
    }

    override fun onDetach() {
        super.onDetach()
        Timber.d("LifeCycle Fragment--- Notes Fragment : onDetach")
    }
}