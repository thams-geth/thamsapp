package com.tts.thamsapp.view.notes.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tts.thamsapp.data.local.entity.NotesEntity
import com.tts.thamsapp.databinding.ItemNotesBinding

class NotesAdapter(private val list: List<NotesEntity>) : RecyclerView.Adapter<NotesViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotesViewHolder {
        val binding = ItemNotesBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return NotesViewHolder(binding)
    }

    override fun onBindViewHolder(holder: NotesViewHolder, position: Int) {
//        holder.setNote(list[position], onNoteClick)
    }

    override fun getItemCount(): Int {
        return list.size
    }
}

class NotesViewHolder(private val view: ItemNotesBinding) : RecyclerView.ViewHolder(view.root) {
    fun setNote(note: NotesEntity, onNoteClick: (Long) -> Unit) {
        view.noteData = note
        view.root.setOnClickListener {
            onNoteClick.invoke(note.id ?: 0)
        }
    }
}