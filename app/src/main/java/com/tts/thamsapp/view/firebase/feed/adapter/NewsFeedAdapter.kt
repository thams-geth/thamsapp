package com.tts.thamsapp.view.firebase.feed.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.tts.thamsapp.data.model.NewsFeedData
import com.tts.thamsapp.databinding.ItemNewsBinding

class ImageAdapter(
    private val list: List<NewsFeedData>,
    private val onNoteClick: (NewsFeedData) -> Unit
) : RecyclerView.Adapter<ImageViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        val binding = ItemNewsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ImageViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        holder.setNews(list[position], onNoteClick)
    }

    override fun getItemCount(): Int {
        return list.size
    }
}

class ImageViewHolder(private val view: ItemNewsBinding) : RecyclerView.ViewHolder(view.root) {
    fun setNews(data: NewsFeedData, onNoteClick: (NewsFeedData) -> Unit) {
        view.data = data
        Glide.with(view.ivNews.context).load(data.imageUrl).into(view.ivNews)
        view.root.setOnClickListener {
            onNoteClick.invoke(data)
        }
    }
}