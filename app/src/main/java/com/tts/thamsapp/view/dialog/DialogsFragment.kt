package com.tts.thamsapp.view.dialog

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.Fragment
import com.tts.thamsapp.databinding.DialogSuccessBinding
import com.tts.thamsapp.databinding.FragmentDialogsBinding
import com.tts.thamsapp.utils.toast

class DialogsFragment : Fragment() {

    lateinit var binding: FragmentDialogsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDialogsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
    }

    private fun setListeners() {
        binding.btnSimpleDialog.setOnClickListener {
            showSimpleDialog()
        }
        binding.btnCustomDialog.setOnClickListener {
            showCustomDialog()
        }
    }

    private fun showCustomDialog() {
//        val view = DialogSuccessBinding.inflate(requireActivity().layoutInflater) // or
//        val view = requireActivity().layoutInflater.inflate(R.layout.dialog_success, null) // or
        val view = DialogSuccessBinding.inflate(LayoutInflater.from(requireContext()))
        val customDialog = AlertDialog.Builder(requireContext()).create()
        customDialog.setView(view.root)
//        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        view.btnOk.setOnClickListener {
            toast(requireContext(), "Ok clicked")
            customDialog.dismiss()
        }
        customDialog.show()
    }

    private fun showSimpleDialog() {
        val dialog = AlertDialog.Builder(requireContext())
            .setTitle("Alert title")
            .setMessage("This is dialog message")
            .setPositiveButton(
                "Yes"
            ) { dialogInterface, i ->
                toast(requireContext(), "This is positive btn")
                dialogInterface.dismiss()
            }
            .setNegativeButton(
                "Cancel"
            ) { dialogInterface, i ->
                toast(requireContext(), "This is negative btn")
            }

        dialog.create().show() // we can use show directly
    }

}