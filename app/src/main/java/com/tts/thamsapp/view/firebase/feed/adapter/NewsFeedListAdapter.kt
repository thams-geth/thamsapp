package com.tts.thamsapp.view.firebase.feed.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.tts.thamsapp.data.model.NewsFeedData
import com.tts.thamsapp.databinding.ItemNewsBinding

class NewsFeedListAdapter(private val onNoteClick: (NewsFeedData) -> Unit) :
    ListAdapter<NewsFeedData, ImageViewHolder>(NotesDiffUtils) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        return ImageViewHolder(
            ItemNewsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        holder.setNews(getItem(position), onNoteClick)
    }

    object NotesDiffUtils : DiffUtil.ItemCallback<NewsFeedData>() {
        override fun areItemsTheSame(oldItem: NewsFeedData, newItem: NewsFeedData): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: NewsFeedData, newItem: NewsFeedData): Boolean {
            return oldItem == newItem
        }

    }
}

