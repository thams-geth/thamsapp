package com.tts.thamsapp.view.service

import android.app.*
import android.content.Intent
import android.media.MediaPlayer
import android.os.Build
import android.os.IBinder
import android.provider.Settings
import androidx.core.app.NotificationCompat
import com.tts.thamsapp.R
import com.tts.thamsapp.utils.Constants
import com.tts.thamsapp.view.home.HomeActivity


class MediaService : Service() {
    val CHANNEL_ID = "ForegroundServiceChannel"

    lateinit var mediaPlayer: MediaPlayer

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val title = intent?.getStringExtra("title")
        mediaPlayer = MediaPlayer.create(this, Settings.System.DEFAULT_RINGTONE_URI)
        mediaPlayer.isLooping = true
        mediaPlayer.start()


//        createNotification()

        val notificationManager =
            applicationContext.getSystemService(NOTIFICATION_SERVICE) as NotificationManager

        val notificationIntent = Intent(this, HomeActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(
            this, 0, notificationIntent, 0
        )

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val serviceChannel = NotificationChannel(
                CHANNEL_ID,
                "Foreground Service Channel",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            val manager = getSystemService(NotificationManager::class.java)
            manager.createNotificationChannel(serviceChannel)
        }
        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentTitle(title)
            .setContentText("Service notification content")
            .setDefaults(NotificationCompat.DEFAULT_ALL)
            .setPriority(NotificationCompat.PRIORITY_MAX)
            .setContentIntent(pendingIntent)
            .build()

        startForeground(1, notification)

//        notificationManager.notify(Random().nextInt(), notification)
        // this lead to crash because we didn't called startForeground when we start the service
        //                     requireContext().startForegroundService(intent)


        val myIntent = Intent(Constants.MY_BROADCAST_RECEIVER_ACTION);
        myIntent.putExtra("name", "this is sample text send from service")
        myIntent.putExtra(Constants.RESULT, Activity.RESULT_OK)
        sendBroadcast(myIntent)

        return START_STICKY
    }

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    private fun createNotification() {


    }

    override fun onDestroy() {
        super.onDestroy()
        mediaPlayer.stop()
    }
}