package com.tts.thamsapp.view.service

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.tts.thamsapp.databinding.FragmentServiceBinding
import com.tts.thamsapp.utils.Constants
import com.tts.thamsapp.utils.showToast


class ServiceFragment : Fragment(), View.OnClickListener {

    lateinit var binding: FragmentServiceBinding

    private val myBroadcast: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val bundle = intent.extras
            if (bundle != null) {
                val resultCode = bundle.getInt(Constants.RESULT)
                if (resultCode == Activity.RESULT_OK) {
                    requireContext().showToast(bundle.getString("name") ?: "")
                }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentServiceBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnStartService.setOnClickListener(this)
        binding.btnStopService.setOnClickListener(this)
    }

    override fun onResume() {
        super.onResume()
        requireContext().registerReceiver(
            myBroadcast,
            IntentFilter(Constants.MY_BROADCAST_RECEIVER_ACTION)
        )
    }

    override fun onPause() {
        super.onPause()
        requireContext().unregisterReceiver(myBroadcast)
    }

    override fun onClick(view: View?) {
        when (view) {
            binding.btnStartService -> {
                val intent = Intent(requireContext(), MediaService::class.java)
                intent.putExtra("title", "Notification title from service fragment")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    requireContext().startForegroundService(intent)
                } else {
                    requireContext().startService(intent)
                }
                requireContext().showToast("Service started")

            }
            binding.btnStopService -> {
                val intent = Intent(requireContext(), MediaService::class.java)
                requireContext().stopService(intent)
                requireContext().showToast("Service stopped")
            }
        }
    }


}