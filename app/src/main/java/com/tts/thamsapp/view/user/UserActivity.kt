package com.tts.thamsapp.view.user

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.tts.thamsapp.databinding.ActivityUserBinding
import timber.log.Timber

class UserActivity : AppCompatActivity() {
    lateinit var binding: ActivityUserBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Timber.d("LifeCycle Activity--- UserActivity : onCreate")
        binding = ActivityUserBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    override fun onStart() {
        super.onStart()
        Timber.d("LifeCycle Activity--- UserActivity : onStart")
    }

    override fun onResume() {
        super.onResume()
        Timber.d("LifeCycle Activity--- UserActivity : onResume")
    }

    override fun onRestart() {
        super.onRestart()
        Timber.d("LifeCycle Activity--- UserActivity : onRestart")
    }

    override fun onPause() {
        super.onPause()
        Timber.d("LifeCycle Activity--- UserActivity : onPause")
    }

    override fun onStop() {
        super.onStop()
        Timber.d("LifeCycle Activity--- UserActivity : onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        Timber.d("LifeCycle Activity--- UserActivity : onDestroy")
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        Timber.d("LifeCycle Activity--- UserActivity : onSaveInstanceState")
    }


    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        Timber.d("LifeCycle Activity--- UserActivity : onRestoreInstanceState")
    }

}