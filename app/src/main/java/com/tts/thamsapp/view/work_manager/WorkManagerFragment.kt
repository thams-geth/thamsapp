package com.tts.thamsapp.view.work_manager

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.work.*
import com.tts.thamsapp.databinding.FragmentWorkManagerBinding
import com.tts.thamsapp.utils.toast
import java.util.concurrent.TimeUnit


class WorkManagerFragment : Fragment() {

    lateinit var binding: FragmentWorkManagerBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentWorkManagerBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
        observeLiveData()
    }

    private fun setListeners() {
        binding.btnStartWM.setOnClickListener {
            startWorkManager()
        }
    }

    private fun startWorkManager() {
        val simpleWorkRequest: WorkRequest =
            OneTimeWorkRequest.from(ShowNotificationWorker::class.java)
        val saveRequest = PeriodicWorkRequestBuilder<ShowNotificationWorker>(1, TimeUnit.HOURS)
            // Additional configuration
            .build()

        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .setRequiresCharging(false)
            .build()

        // After 5 sec the notification will pop up even after we close the app
        val addNoteWithBuilder: WorkRequest = OneTimeWorkRequestBuilder<ShowNotificationWorker>()
            .setConstraints(constraints)
            .setInitialDelay(5, TimeUnit.SECONDS)
            .setInputData(workDataOf("data1" to "Add Note automatically"))
            .addTag("noteTag")
            .build()


        WorkManager.getInstance(requireContext()).enqueue(addNoteWithBuilder)

        val workManager = WorkManager.getInstance(requireContext())
        workManager.getWorkInfosByTag("noteTaj")
        workManager.getWorkInfoByIdLiveData(addNoteWithBuilder.id)
            .observe(viewLifecycleOwner, {
                when (it.state) {
                    WorkInfo.State.SUCCEEDED -> {
                        toast(requireContext(), "Work Succeeded with o/p data : + ${it.outputData}")
                    }
                    WorkInfo.State.ENQUEUED -> toast(requireContext(), "Work ENQUEUED")
                    WorkInfo.State.RUNNING -> toast(requireContext(), "Work RUNNING")
                    WorkInfo.State.FAILED -> toast(requireContext(), "Work FAILED")
                    WorkInfo.State.BLOCKED -> toast(requireContext(), "Work BLOCKED")
                    WorkInfo.State.CANCELLED -> toast(requireContext(), "Work CANCELLED")
                    else -> toast(requireContext(), "Work in else branch")
                }
            })
    }

    private fun observeLiveData() {

    }

}