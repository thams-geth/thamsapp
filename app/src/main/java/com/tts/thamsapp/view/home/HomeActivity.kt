package com.tts.thamsapp.view.home

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.onNavDestinationSelected
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.tts.thamsapp.R
import com.tts.thamsapp.databinding.ActivityHomeBinding
import timber.log.Timber

class HomeActivity : AppCompatActivity() {
    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityHomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Timber.d("LifeCycle Activity--- Home Activity : onCreate ")
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)

        val navController = findNavController(R.id.navHost)
//        appBarConfiguration = AppBarConfiguration(navController.graph, binding.drawerLayout)
        val mainMenuList = setOf(
            R.id.homeFragment,
            R.id.notesFragment,
            R.id.workManagerFragment,
            R.id.dialogsFragment,
            R.id.broadCastFragment,
            R.id.serviceFragment
        )
        appBarConfiguration = AppBarConfiguration(mainMenuList, binding.drawerLayout)
        setupActionBarWithNavController(navController, appBarConfiguration)
        binding.navView.setupWithNavController(navController)
        binding.bottomNav.setupWithNavController(navController)

        navController.addOnDestinationChangedListener(destinationChangedListener)
//        binding.navView.setNavigationItemSelectedListener(navItemSelectedListener)
        // No need to add item selected listeners , android system automatically handle
        // nav, because we set menu id as nav graph id
        // binding.bottomNav.setOnItemSelectedListener(bottomNavItemSelectedListener)
    }

    private val destinationChangedListener =
        NavController.OnDestinationChangedListener { controller, destination, arguments ->
            when (destination.id) {
                R.id.homeFragment -> binding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
                R.id.notesFragment -> binding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
                R.id.workManagerFragment -> binding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
                R.id.dialogsFragment -> binding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
                R.id.broadCastFragment -> binding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
                R.id.serviceFragment -> binding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
                else -> binding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
            }
        }
    /* private val navItemSelectedListener = NavigationView.OnNavigationItemSelectedListener {
         // clear stack up to home fragment A-b-c-d , will clear b c d and start new d so a-d
         // if true means home fragment also cleared
         findNavController(R.id.navHost).popBackStack(R.id.homeFragment, false)
         when (it.itemId) {
             R.id.homeFragment -> {
                 if (findNavController(R.id.navHost).currentDestination?.id != R.id.homeFragment)
                     findNavController(R.id.navHost).navigate(R.id.homeFragment)
             }
             R.id.notesFragment -> findNavController(R.id.navHost).navigate(R.id.notesFragment)
             R.id.dialogsFragment -> findNavController(R.id.navHost).navigate(R.id.dialogsFragment)
             R.id.workManagerFragment -> findNavController(R.id.navHost).navigate(R.id.workManagerFragment)
         }
         binding.drawerLayout.closeDrawer(GravityCompat.START)
         true
     }*/

    /* private val bottomNavItemSelectedListener = NavigationBarView.OnItemSelectedListener {
         when (it.itemId) {
             R.id.homeFragment -> findNavController(R.id.navHost).navigate(R.id.homeFragment)
             R.id.workManagerFragment -> findNavController(R.id.navHost).navigate(R.id.workManagerFragment)
         }
         true
     }*/

    override fun onStart() {
        super.onStart()
        Timber.d("LifeCycle Activity--- Home Activity : onStart")
    }

    override fun onResume() {
        super.onResume()
        Timber.d("LifeCycle Activity--- Home Activity : onResume")
    }

    override fun onRestart() {
        super.onRestart()
        Timber.d("LifeCycle Activity--- Home Activity : onRestart")
    }

    override fun onPause() {
        super.onPause()
        Timber.d("LifeCycle Activity--- Home Activity : onPause")
    }

    override fun onStop() {
        super.onStop()
        Timber.d("LifeCycle Activity--- Home Activity : onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        Timber.d("LifeCycle Activity--- Home Activity : onDestroy")
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        Timber.d("LifeCycle Activity--- Home Activity : onSaveInstanceState")
    }


    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        Timber.d("LifeCycle Activity--- Home Activity : onRestoreInstanceState")
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return item.onNavDestinationSelected(findNavController(R.id.navHost))
                || super.onOptionsItemSelected(item)
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.navHost)
        return navController.navigateUp(appBarConfiguration)
                || super.onSupportNavigateUp()
    }

    companion object {
        val name: String = HomeActivity::class.java.name
    }
}