package com.tts.thamsapp.view.firebase.work_manager

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.NotificationManager.IMPORTANCE_HIGH
import android.app.PendingIntent
import android.content.Context
import android.content.Context.NOTIFICATION_SERVICE
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build.VERSION.SDK_INT
import android.os.Build.VERSION_CODES.O
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationCompat.PRIORITY_MAX
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import com.tts.thamsapp.R
import com.tts.thamsapp.data.model.NewsFeedData
import com.tts.thamsapp.view.home.HomeActivity
import java.io.ByteArrayOutputStream
import java.util.Random

class UploadNewsWorker(private val context: Context, workerParameters: WorkerParameters) :
    Worker(context, workerParameters) {

    companion object {
        private const val NOTIFICATION_NAME = "UploadNews"
        private const val NOTIFICATION_CHANNEL = "Upload"
    }

    private val storageRef = Firebase.storage.reference
    private val db = Firebase.firestore


    override fun doWork(): Result {
        return try {
//            val callBack = { success: Boolean ->
//                if (success) Result.success() else Result.failure()
//            }
            upload()
        } catch (e: Exception) {
            Result.failure()
        }
    }

    private fun upload(): Result {
        val imagePath = inputData.getString("imagePath")
        val bitmap = BitmapFactory.decodeFile(imagePath)
        val byteArray = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 75, byteArray)
        val data = byteArray.toByteArray()

        val imageRef = storageRef.child("IMG_${System.currentTimeMillis()}.jpg")
        imageRef.putBytes(data).continueWithTask { task ->
            if (!task.isSuccessful) {
                task.exception?.let {
                    throw it
                }
                showNotification("Oops! Something went wrong.News didn't published.")
            }
            imageRef.downloadUrl
        }.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val downloadUri = task.result
                val newsFeedData = NewsFeedData(
                    title = inputData.getString("title"),
                    content = inputData.getString("content"),
                    imageUrl = downloadUri.toString(),
                )
                db.collection("news").add(newsFeedData).addOnSuccessListener {
                    showNotification("News published successfully.")

                }.addOnFailureListener {
                    showNotification("Oops! Something went wrong.News didn't published.")
                }
            } else {
                showNotification("Oops! Something went wrong.News didn't published.")
            }
        }.result
        return Result.success()
    }

    private fun showNotification(message: String) {
        val intent = Intent(context, HomeActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        val notificationManager = applicationContext.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        val pendingIntent = PendingIntent.getActivity(context, 0, intent, 0)
        val notification = NotificationCompat.Builder(context, NOTIFICATION_CHANNEL)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentTitle("News")
            .setContentText(message)
            .setPriority(PRIORITY_MAX)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)

        if (SDK_INT >= O) {
            val channel = NotificationChannel(NOTIFICATION_CHANNEL, NOTIFICATION_NAME, IMPORTANCE_HIGH)
            notificationManager.createNotificationChannel(channel)
        }
        notificationManager.notify(Random().nextInt(), notification.build())
    }
}
