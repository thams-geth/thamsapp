package com.tts.thamsapp.view.notes

import com.tts.thamsapp.data.local.entity.NotesEntity

object NoteValidator {
    fun validate(note: NotesEntity): Boolean {
        return (note.title.isNotEmpty() && note.desc.isNotEmpty())
    }
}