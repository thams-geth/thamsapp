package com.tts.thamsapp

import android.app.Application
import com.tts.thamsapp.di.databaseModule
import com.tts.thamsapp.di.repositoryModule
import com.tts.thamsapp.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class ThamsApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
        startKoin {
            androidContext(this@ThamsApplication)
            modules(listOf(viewModelModule, repositoryModule, databaseModule))
        }
    }
}