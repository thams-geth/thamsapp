package com.tts.thamsapp.di

import com.tts.thamsapp.view.firebase.create_news.CreateNewsViewModel
import com.tts.thamsapp.view.firebase.feed.NewsFeedViewModel
import com.tts.thamsapp.viewmodel.NotesViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


val viewModelModule = module {
    viewModel { NotesViewModel(get()) }
    viewModel { NewsFeedViewModel() }
    viewModel { CreateNewsViewModel() }
}

val repositoryModule = module {
    single { provideNotesRepository(get()) }
}

val databaseModule = module {
    single { provideDatabase(androidApplication()) }
    single { provideNotesDao(get()) }
}