package com.tts.thamsapp.di

import com.tts.thamsapp.data.local.dao.NotesDao
import com.tts.thamsapp.data.repository.NotesRepository

fun provideNotesRepository(notesDao: NotesDao): NotesRepository {
    return NotesRepository(notesDao)
}

