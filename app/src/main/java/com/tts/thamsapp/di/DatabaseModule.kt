package com.tts.thamsapp.di

import android.content.Context
import androidx.room.Room
import com.tts.thamsapp.data.local.ThamsDatabase
import com.tts.thamsapp.data.local.dao.NotesDao

fun provideDatabase(context: Context): ThamsDatabase {
    return Room.databaseBuilder(context, ThamsDatabase::class.java, "thamsdatabase.db").build()
}

fun provideNotesDao(db: ThamsDatabase): NotesDao {
    return db.notesDao()
}