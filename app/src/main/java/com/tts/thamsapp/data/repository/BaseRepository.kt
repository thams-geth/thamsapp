package com.tts.thamsapp.data.repository

import android.accounts.NetworkErrorException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.StringWriter
import java.net.UnknownHostException

open class BaseRepository {

    suspend fun <T> safeApiCall(
        apiCall: suspend () -> T
    ): Results<T> {
        return withContext(Dispatchers.IO) {
            try {
                Results.Success(apiCall.invoke())
            } catch (e: Exception) {
                when (e) {

                    is NetworkErrorException -> {
                        val errorMessage = e.message ?: ""
                        Results.Failure(0, ErrorStatus.NO_CONNECTION, errorMessage)
                    }
                    is UnknownHostException -> {
                        val errorMessage = e.message ?: ""
                        Results.Failure(0, ErrorStatus.NO_CONNECTION, errorMessage)
                    }
                    else -> {
                        // val errorMessage = e.localizedMessage ?: ""
                        val sw = StringWriter()
                        val sStackTrace: String = sw.toString() // stack trace as a string
                        println(sStackTrace)
                        Results.Failure(0, ErrorStatus.UNKNOWN, sStackTrace)
                    }
                }
            }
        }
    }
}
