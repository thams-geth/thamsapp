package com.tts.thamsapp.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.tts.thamsapp.data.local.dao.NotesDao
import com.tts.thamsapp.data.local.entity.NotesEntity

class NotesRepository(private val notesDao: NotesDao) : BaseRepository() {

    fun addNote(notes: NotesEntity): Long {
        return notesDao.insertNote(notes)
    }

    fun getNotes(): LiveData<List<NotesEntity>> {
        return notesDao.getNotes()
    }

    fun getNoteById(id: Long): NotesEntity {
        return notesDao.getNotesById(id)
    }

    fun updateNotesById(title: String, desc: String, id: Long): Int {
        return notesDao.updateNotesById(title = title, desc = desc, noteId = id)
    }

    fun deleteNoteById(id: Long): Int {
        return notesDao.deleteNoteById(noteId = id)
    }
}