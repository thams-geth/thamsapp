package com.tts.thamsapp.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.tts.thamsapp.data.local.dao.NotesDao
import com.tts.thamsapp.data.local.entity.NotesEntity

@Database(entities = [NotesEntity::class], version = 1, exportSchema = false)
abstract class ThamsDatabase : RoomDatabase() {
    abstract fun notesDao(): NotesDao
}