package com.tts.thamsapp.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.tts.thamsapp.data.local.entity.NotesEntity

@Dao
interface NotesDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertNote(notesEntity: NotesEntity): Long

    @Query("SELECT * FROM thams_notes")
    fun getNotes(): LiveData<List<NotesEntity>>

    @Query("SELECT * FROM thams_notes WHERE id = :noteId")
    fun getNotesById(noteId: Long): NotesEntity

    @Query("UPDATE thams_notes SET title = :title, `desc` = :desc WHERE id = :noteId")
    fun updateNotesById(title: String, desc: String, noteId: Long): Int
//    fun updateNotesById(notesEntity: NotesEntity): MutableLiveData<NotesEntity>

    @Query("DELETE FROM thams_notes WHERE id=:noteId")
    fun deleteNoteById(noteId: Long): Int

}