package com.tts.thamsapp.data.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class NewsFeedData(
    val id: String? = null,
    val title: String? = null,
    val content: String? = null,
    val imageUrl: String? = null,
) : Parcelable