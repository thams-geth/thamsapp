package com.tts.thamsapp.data.service

import com.google.firebase.firestore.FirebaseFirestore
import com.tts.thamsapp.data.model.NewsFeedData
import com.tts.thamsapp.data.model.UserData
import com.tts.thamsapp.data.model.UserData.Companion.toUserData
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow

object FirebaseService {

    fun getProfileData(userId: String): UserData? {
        val db = FirebaseFirestore.getInstance()
        return try {
            db.collection("users").document(userId).get().result.toUserData()
        } catch (e: Exception) {
//            FirebaseCrashlytics.getInstance().log("Error getting user details")
//            FirebaseCrashlytics.getInstance().setCustomKey("user id", xpertSlug)
//            FirebaseCrashlytics.getInstance().recordException(e)
            null
        }
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    fun getNewsFeed(): Flow<List<NewsFeedData>> {
        val db = FirebaseFirestore.getInstance()
        return callbackFlow {
            val list = db.collection("news")
                .addSnapshotListener { value, e ->
                    if (e != null) {
                        return@addSnapshotListener
                    }
                    val map = value?.documents?.mapNotNull {
                        it.toObject(NewsFeedData::class.java)
                    }
                    map?.let { this.trySend(it) }
                }
            awaitClose { list.remove() }
        }
    }

    fun getFriends(userId: String): List<UserData> {
        val db = FirebaseFirestore.getInstance()
        return try {
            db.collection("users")
                .document(userId)
                .collection("friends").get().result
                .documents.mapNotNull { it.toUserData() }
        } catch (e: Exception) {
//            FirebaseCrashlytics.getInstance().log("Error getting user friends")
//            FirebaseCrashlytics.getInstance().setCustomKey("user id", xpertSlug)
//            FirebaseCrashlytics.getInstance().recordException(e)
            emptyList()
        }
    }

    fun getData() {
        val imageDataList = ArrayList<NewsFeedData>()
        val db = FirebaseFirestore.getInstance()
        db.collection("images")
            .addSnapshotListener { value, e ->
                if (e != null) {
                    return@addSnapshotListener
                }
                imageDataList.clear()
                if (value != null && value.size() > 0) {
                    for (doc in value) {
                        imageDataList.add(doc.toObject(NewsFeedData::class.java))
                    }
                }
            }
    }
}