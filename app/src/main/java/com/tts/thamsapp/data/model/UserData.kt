package com.tts.thamsapp.data.model

import com.google.firebase.firestore.DocumentSnapshot
import timber.log.Timber

data class UserData(
    val userId: String? = null,
    val name: String? = null,
    val phoneNo: String? = null,
    val bio: String? = null,
) {
    companion object {
        fun DocumentSnapshot.toUserData(): UserData? {
            return try {
                val name = getString("name")
                val phoneNo = getString("phoneNo")
                val bio = getString("bio")
                UserData(id, name, phoneNo, bio)
            } catch (e: Exception) {
                Timber.e(e, "Error converting user profile")
//                FirebaseCrashlytics.getInstance().log("Error converting user profile")
//                FirebaseCrashlytics.getInstance().setCustomKey("userId", id)
//                FirebaseCrashlytics.getInstance().recordException(e)
                null
            }
        }

        private const val TAG = "User"
    }
}
