package com.tts.thamsapp.data.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "thams_notes")
data class NotesEntity(
    @PrimaryKey(autoGenerate = true)
    var id: Long? = null,
    val title: String,
    val desc: String,
)